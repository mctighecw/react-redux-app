# README

This is a tiny React app that uses Redux and some middleware.

## App Information

App Name: react-redux-app

Created: September 2017

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-redux-app)

## Components

1. counter
2. form
3. to do list

## Notes

* React
* React Router
* Redux
* Middleware: promise, thunk, logger
* LESS

Last updated: 2024-11-24
