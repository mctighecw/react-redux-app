import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { counterIncrease, counterDecrease, counterReset } from '../redux/actions';

class Counter extends Component {
  constructor(props) {
    super(props);
  }

  onClickBack = () => this.props.history.goBack();

  render() {
    let { counter, onClickIncrease, onClickDecrease, onClickReset } = this.props;

    return (
      <div>
        <h1>Counter</h1>
        <h3>Count: <b>{counter}</b></h3>
        <button onClick={onClickIncrease}>+</button>
        <button onClick={onClickDecrease}>-</button>
        <button onClick={onClickReset}>reset</button>
        <hr />
        <button onClick={this.onClickBack}>back</button>
      </div>
    );
  }
}

Counter.propTypes = {
  counter: PropTypes.number.isRequired,
  onClickIncrease: PropTypes.func.isRequired,
  onClickDecrease: PropTypes.func.isRequired,
  onClickReset: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    counter: state.counter
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onClickIncrease: () => {dispatch(counterIncrease())},
    onClickDecrease: () => {dispatch(counterDecrease())},
    onClickReset: () => {dispatch(counterReset())}
  }
}

const CounterConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter)

export default CounterConnect;
