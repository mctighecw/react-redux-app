import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Home extends Component {
  render() {
    return (
      <div>
        <h1>Redux Components</h1>
        <br />
        <Link to='/counter'>Counter</Link>
        <br />
        <Link to='/form'>Form</Link>
        <br />
        <Link to='/todolist'>To Do List</Link>
      </div>
    );
  }
}

export default Home;
