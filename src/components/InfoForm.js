import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { formAddName, formAddEmail, formClear } from '../redux/actions';

class InfoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputName: '',
      inputEmail: ''
    };
  }

  onClickBack = () => this.props.history.goBack();

  handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState({
      [name]: value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let { inputName, inputEmail } = this.state;
    let { onClickAddName, onClickAddEmail } = this.props;

    if (inputName !== '' && inputEmail !== '') {
      onClickAddName(inputName);
      onClickAddEmail(inputEmail);

      this.setState({
        inputName: '',
        inputEmail: ''
      });
    } else {
      alert('Be sure to fill out both form fields!');
    }
  }

  render() {
    let { inputName, inputEmail } = this.state;
    let { name, email, onClickAddName, onClickClear } = this.props;

    return (
      <div>
        <h1>Form Input</h1>
        {name && email ? (
          <div style={{marginBottom: 10}}>
            <p style={{marginBottom: 7}}><i>Current values:</i><br />
            Name: {name}<br />
            Email: {email}</p>
            <button onClick={onClickClear} style={{marginLeft: 0}}>clear</button>
          </div>
        ) : null }
        <form onSubmit={(e) => {this.handleSubmit(e)}}>
          <label>
            <span>Name:</span>
            <input type="text" value={inputName} name="inputName" onChange={(e) => {this.handleInputChange(e)}} />
          </label>
          <label>
            <span>Email:</span>
            <input type="text" value={inputEmail} name="inputEmail" onChange={(e) => {this.handleInputChange(e)}} />
          </label>
          <input type="submit" value="Submit" />
        </form>
        <hr />
        <button onClick={this.onClickBack}>back</button>
      </div>
    );
  }
}

InfoForm.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  onClickAddName: PropTypes.func.isRequired,
  onClickAddEmail: PropTypes.func.isRequired,
  onClickClear: PropTypes.func.isRequired
};

const mapStateToProps = state => {
  return {
    name: state.form.name,
    email: state.form.email
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onClickAddName: (name) => {dispatch(formAddName(name))},
    onClickAddEmail: (email) => {dispatch(formAddEmail(email))},
    onClickClear: () => {dispatch(formClear())}
  }
}

const InfoFormConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoForm)

export default InfoFormConnect;
