import React, { Component } from 'react';
import AddToDo from './containers/AddToDo';
import VisibleToDoList from './containers/VisibleToDoList';
import Footer from './components/Footer';

class ToDoMain extends Component {

  onClickBack = () => this.props.history.goBack();

  render() {
    return (
      <div>
        <h1>To Do List</h1>
        <AddToDo />
        <VisibleToDoList />
        <br />
        <Footer />
        <hr />
        <button onClick={this.onClickBack}>back</button>
      </div>
    )
  }
}

export default ToDoMain;
