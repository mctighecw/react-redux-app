import React from 'react';
import { connect } from 'react-redux';
import { addTodo, clearFinished, clearAllTodos } from '../../../redux/actions';

let AddTodo = ({ dispatch }) => {
  let input;

  return (
    <div>
      <form onSubmit={(e) => {
        e.preventDefault()
        if (!input.value.trim()) {
          return
        }
        dispatch(addTodo(input.value))
        input.value = ''
      }}>
        <input ref={node => {
          input = node
        }} />
        <button type='submit'>
          Add Todo
        </button>
        <button type='button' onClick={(e) => {
          dispatch(clearFinished())
        }}>
          Clear Finished
        </button>
        <button type='button' onClick={(e) => {
          dispatch(clearAllTodos())
        }}>
          Clear All
        </button>
      </form>
    </div>
  )
};
AddTodo = connect()(AddTodo);

export default AddTodo;
