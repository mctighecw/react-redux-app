import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './history';
import './styles/styles.less';

import Home from './components/Home';
import Counter from './components/Counter';
import InfoForm from './components/InfoForm';
import ToDoMain from './components/todolist/ToDoMain';

const Routes = () => (
  <Router history={history}>
    <Switch>
      <Route exact path='/' component={Home} />
      <Route path='/counter' component={Counter} />
      <Route path='/form' component={InfoForm} />
      <Route path='/todolist' component={ToDoMain} />
      <Route path='*' component={Home} />
    </Switch>
  </Router>
);

class App extends Component {
  render() {
    return (
      <Routes />
    );
  }
}

export default App;
