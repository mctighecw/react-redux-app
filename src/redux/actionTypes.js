// counter
export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const RESET = 'RESET';

// form
export const ADD_NAME = 'ADD_NAME';
export const ADD_EMAIL = 'ADD_EMAIL';
export const CLEAR_FORM = 'CLEAR_FORM';

// to do list
export const ADD_TODO = 'ADD_TODO';
export const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const CLEAR_FINISHED = 'CLEAR_FINISHED';
export const CLEAR_TODOS = 'CLEAR_TODOS';
