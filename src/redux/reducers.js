import { combineReducers } from 'redux';

import {
  INCREMENT,
  DECREMENT,
  RESET,
  ADD_NAME,
  ADD_EMAIL,
  CLEAR_FORM,
  ADD_TODO,
  SET_VISIBILITY_FILTER,
  TOGGLE_TODO,
  CLEAR_FINISHED,
  CLEAR_TODOS
} from './actionTypes';

/* counter reducer */
function counter(state = 0, action) {
  switch (action.type) {
    case INCREMENT:
      return state + 1
    case DECREMENT:
      return state - 1
    case RESET:
      return 0
    default:
      return state
  }
};

/* form reducer */
const formInitialState = {
   name: '',
   email: ''
};

function form(state = formInitialState, action) {
  switch (action.type) {
    case ADD_NAME:
      return Object.assign({}, state, {
        name: action.name
      })
    case ADD_EMAIL:
      return Object.assign({}, state, {
        email: action.email
      })
    case CLEAR_FORM:
      return Object.assign({}, state, {
        name: '',
        email: ''
      })
    default:
      return state
  }
};

/* to do list reducers */
function todo(state = {}, action) {
  switch (action.type) {
    case ADD_TODO:
      return {
        id: action.id,
        text: action.text,
        completed: false
      }
    case TOGGLE_TODO:
      if (state.id !== action.id) {
        return state
      }
      return Object.assign({}, state, {
        completed: !state.completed
      })
    default:
      return state
  }
};

function todos(state = [], action) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        todo(undefined, action)
      ]
    case TOGGLE_TODO:
      return state.map(t =>
        todo(t, action)
      )
    case CLEAR_FINISHED:
      return state.filter(function(todo){
        return !todo.completed
      })
    case CLEAR_TODOS:
      return []
    default:
      return state
  }
};

function visibilityFilter(state = 'SHOW_ALL', action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter
    default:
      return state
  }
};

const rootReducer = combineReducers({
  counter,
  form,
  todos,
  visibilityFilter
});

export default rootReducer;
