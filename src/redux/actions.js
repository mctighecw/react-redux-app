import {
  INCREMENT,
  DECREMENT,
  RESET,
  ADD_NAME,
  ADD_EMAIL,
  CLEAR_FORM,
  ADD_TODO,
  SET_VISIBILITY_FILTER,
  TOGGLE_TODO,
  CLEAR_FINISHED,
  CLEAR_TODOS
} from './actionTypes';

// counter
export function counterIncrease() {
  return {
    type: INCREMENT
  }
};

export function counterDecrease() {
  return {
    type: DECREMENT
  }
};

export function counterReset() {
  return {
    type: RESET
  }
};

// form
export function formAddName(name) {
  return {
    type: ADD_NAME,
    name
  }
};

export function formAddEmail(email) {
  return {
    type: ADD_EMAIL,
    email
  }
};

export function formClear() {
  return {
    type: CLEAR_FORM
  }
};

// to do list
let nextTodoId = 0;

export const addTodo = (text) => {
  return {
    type: ADD_TODO,
    id: nextTodoId++,
    text
  }
};

export const setVisibilityFilter = (filter) => {
  return {
    type: SET_VISIBILITY_FILTER,
    filter
  }
};

export const toggleTodo = (id) => {
  return {
    type: TOGGLE_TODO,
    id
  }
};

export const clearFinished = () => {
  return {
    type: CLEAR_FINISHED
  }
};

export const clearAllTodos = () => {
  return {
    type: CLEAR_TODOS
  }
};
