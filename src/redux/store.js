import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import promise from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './reducers';

const reduxDevTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

let composed = (process.env.NODE_ENV === 'development') ? (
    compose(
      applyMiddleware(
        promise(),
        thunk,
        logger
      ),
      autoRehydrate(),
      reduxDevTools
    )
  ) : (
    compose(
      applyMiddleware(
        promise(),
        thunk
      ),
      autoRehydrate()
    )
);

const store = createStore(
  rootReducer,
  composed
);

persistStore(store);

export default store;
